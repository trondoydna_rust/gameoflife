mod cell;

use cell::CellGrid;
use cell::CellState::*;

fn main() {
    let mut grid = CellGrid::new(vec![
        vec![ALIVE, DEAD, ALIVE],
        vec![ALIVE, DEAD, ALIVE],
        vec![ALIVE, DEAD, ALIVE],
    ]);

    grid.iter()
        .for_each(|grid| println!("{}", grid));
}
