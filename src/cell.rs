use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Error;

#[derive(Debug, PartialEq)]
pub enum CellState {
    DEAD,
    ALIVE
}

#[derive(Debug, PartialEq)]
pub struct CellGrid {
    cells: Vec<Vec<CellState>>
}

pub struct CellGridIter<'a> {
    grid: &'a CellGrid,
    previous_grid: Option<&'a CellGrid>
}

impl CellGrid {

    pub fn new(cells: Vec<Vec<CellState>>) -> CellGrid {
        CellGrid { cells }
    }

    pub fn iter(&self) -> CellGridIter {
        CellGridIter { grid: self, previous_grid: Option::None }
    }

    pub fn new_state(&self, row_index: isize, column_index: isize) -> Option<CellState> {
        let state = self.find_cell_state(row_index, column_index)?;

        Option::Some(match state {
            CellState::DEAD => self.new_state_from_dead(row_index, column_index),
            CellState::ALIVE => self.new_state_from_alive(row_index, column_index)
        })
    }

    fn new_state_from_dead(&self, row_index: isize, column_index: isize) -> CellState {
        let neighbor_count = self.count_alive_neighbors(row_index, column_index);

        match neighbor_count {
            3 => CellState::ALIVE,
            _ => CellState::DEAD
        }
    }

    fn new_state_from_alive(&self, row_index: isize, column_index: isize) -> CellState {
        let neighbor_count = self.count_alive_neighbors(row_index, column_index);

        match neighbor_count {
            2 | 3 => CellState::ALIVE,
            _ => CellState::DEAD
        }
    }

    fn count_alive_neighbors(&self, row_index: isize, column_index: isize) -> usize {
        self.find_neighbors_state(row_index, column_index).iter()
            .filter(|o| o.is_some())
            .map(|o| o.unwrap())
            .filter(|&c| c == &CellState::ALIVE)
            .count()
    }

    fn find_neighbors_state(&self, row_index: isize, column_index: isize) -> Vec<Option<&CellState>> {
        let mut neighbors = Vec::new();

        neighbors.push( self.find_cell_state(row_index - 1, column_index -1));
        neighbors.push( self.find_cell_state(row_index - 1, column_index));
        neighbors.push( self.find_cell_state(row_index - 1, column_index + 1));

        neighbors.push( self.find_cell_state(row_index, column_index - 1));
        neighbors.push( self.find_cell_state(row_index, column_index + 1));

        neighbors.push( self.find_cell_state(row_index + 1, column_index - 1));
        neighbors.push( self.find_cell_state(row_index + 1, column_index));
        neighbors.push( self.find_cell_state(row_index + 1, column_index + 1));

        neighbors
    }

    fn find_cell_state(&self, row_index: isize, column_index: isize) -> Option<&CellState> {
        if row_index < 0 || column_index < 0 {
            return Option::None;
        }

        match self.cells.get(row_index as usize) {
            Option::None => Option::None,
            Option::Some(r) => match r.get(column_index as usize) {
                Option::None => Option::None,
                Option::Some(c) => Option::Some(c)
            }
        }
    }

}

impl Display for CellGrid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        for row in &self.cells {
            writeln!(f, "{:?}", row)?
        }
        Result::Ok(())
    }
}

impl <'a> Iterator for CellGridIter<'a> {
    type Item = CellGrid;

    fn next(&'a mut self) -> Option<<Self as Iterator>::Item> {
        let mut rows = Vec::new();

        for row_index in 0..self.grid.cells.len() {
            let mut column = Vec::new();

            for column_index in 0..=2 {
                let state = self.grid.new_state(row_index, column_index).unwrap();

                column.push(state);
            }

            rows.push(column);
        }

        let grid = CellGrid::new(rows);

        self.previous_grid = Option::Some(&self.grid);
        self.grid = &grid;

        Some(self.grid)
    }
}

